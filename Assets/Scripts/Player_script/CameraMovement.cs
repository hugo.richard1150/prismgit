﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public Transform target;
    private Vector3 offssetPos;
    [HideInInspector] public Vector2 offsetPosZone;

    public float smoothPos;

    public int focus;

    public GameObject boss1;

    public void Start()
    {
        offssetPos = transform.position - target.position;
    }

    private void FixedUpdate()
    {
        if (!FindObjectOfType<BossTrigger>().cameraToBoss1)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(target.position.x + offsetPosZone.x, target.position.y + offsetPosZone.y, target.position.z - focus) + offssetPos, Time.deltaTime * smoothPos);
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(boss1.transform.position.x, boss1.transform.position.y, boss1.transform.position.z - 20), Time.deltaTime);
        }
    }
}
