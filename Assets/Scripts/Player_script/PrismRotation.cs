﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrismRotation : MonoBehaviour
{
    public float maxSpeed;
    private float curSpeed;

    private float desiredRot = -90;
    public float desiredRotZ;
    public float maxSpeedTime;
    private float lerp;

    private float t;

    public void Update()
    {
        //Rotation prisme vers la gauche
        if (Input.GetKey(KeyCode.A) || Input.GetAxis("RotationLeft") > 0)
        {
            t += Time.deltaTime;

            lerp = Mathf.InverseLerp(0, maxSpeedTime, t);
            curSpeed = Mathf.SmoothStep(0, maxSpeed, lerp);
            desiredRot += curSpeed * Time.deltaTime;

            var desiredRotQ = Quaternion.Euler(desiredRot, 0, desiredRotZ);
            transform.localRotation = desiredRotQ;
        }    
        //Rotation prisme vers la droite
        else if (Input.GetKey(KeyCode.E) || Input.GetAxis("RotationRight") > 0)
        {
            t += Time.deltaTime;

            lerp = Mathf.InverseLerp(0, maxSpeedTime, t);
            curSpeed = Mathf.SmoothStep(0, maxSpeed, lerp);
            desiredRot -= curSpeed * Time.deltaTime;

            var desiredRotQ = Quaternion.Euler(desiredRot, 0, desiredRotZ);
            transform.localRotation = desiredRotQ;
        }
        //Reset des variables quand il n'y a pas d'input
        else
        {
            t = 0f;
            lerp = 0f;
            curSpeed = 0f;
        }
    }
}
