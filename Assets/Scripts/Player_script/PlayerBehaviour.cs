﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBehaviour : MonoBehaviour
{
    [Header ("Player Health")]
    public int health;
    public int maxHealth = 100;
    public Slider healthSlider;

    public Transform respawnPoint;

    //Amélioration de la loupe
    public bool wenActive;

    public void Start()
    {
        health = maxHealth;
        transform.position = respawnPoint.position;
    }

    public void Update()
    {
        healthSlider.value = health;

        if (health <= 0)
        {
            Death();
        }
    }

    public void Damage(int amountDamage)
    {
        health -= amountDamage;
        Debug.Log("Vie joueur : " + health);
    }

    private void Death()
    {
        transform.position = respawnPoint.position;
        health = maxHealth;
    }
}
