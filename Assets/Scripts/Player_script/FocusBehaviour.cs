﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocusBehaviour : MonoBehaviour
{
    public enum FocusCenter { Player, Room }
    public FocusCenter focusType;
    public enum MusicState { Exploration, Puzzle, BossDoor, Boss }
    public MusicState musicState;
    
    // Musique
    private FMODUnity.StudioEventEmitter music;

    public LightActivator activator;

    [Header("changement")]
    public float fovChange;


    /*
    [Header("Focus Distance (Z axis)")]
    public int focusDistance; */

    [Header("Focus Distance (X axis & Y axis)")]
    public Vector2 presetCameraOffset;

    [Header("Focus Distance (Z axis)")]
    public int focusDistance;

    public float smooth;

    void Start()
    {
        music = GameObject.Find("MusicManager").GetComponent<FMODUnity.StudioEventEmitter>();
        Debug.Log(music);
    }

    void Update()
    {
        
    }

    public void OnTriggerStay(Collider other)
    {
         if (other.gameObject.tag == "PlayerBody")
         {
            FindObjectOfType<Camera>().fieldOfView = Mathf.Lerp(FindObjectOfType<Camera>().fieldOfView, fovChange, smooth);
            GameObject.FindObjectOfType<CameraMovement>().focus = focusDistance;

            if (focusType == FocusCenter.Player)
            {
                GameObject.FindObjectOfType<CameraMovement>().target = GameObject.FindGameObjectWithTag("Player").transform;
                GameObject.FindObjectOfType<CameraMovement>().offsetPosZone = presetCameraOffset;
            }

            else if (focusType == FocusCenter.Room)
            {
                GameObject.FindObjectOfType<CameraMovement>().target = gameObject.transform;
                GameObject.FindObjectOfType<CameraMovement>().offsetPosZone = presetCameraOffset;
            }

            CheckMusicState();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "PlayerBody")
        {

            FindObjectOfType<Camera>().fieldOfView = Mathf.Lerp(fovChange, 25f, smooth);
            GameObject.FindObjectOfType<CameraMovement>().target = GameObject.FindGameObjectWithTag("Player").transform;
            GameObject.FindObjectOfType<CameraMovement>().offsetPosZone = new Vector2(0,0) ;

            music.SetParameter("Boss Fight", 0);
            music.SetParameter("Boss Door", 0);
            music.SetParameter("Puzzle", 0);
            music.SetParameter("Puzzle Completed", 0);
        }
    }

    void CheckMusicState()
    {
        if (musicState == MusicState.Puzzle)
        {
            music.SetParameter("Puzzle", 1);
        }
        else if (musicState == MusicState.Boss)
        {
            music.SetParameter("Boss Fight", 1);
            music.SetParameter("Exploration", 1);
            music.SetParameter("Puzzle Completed", 1);
            music.SetParameter("Boss Door", 0);
            music.SetParameter("Puzzle", 0);
        }
        else if (musicState == MusicState.BossDoor)
        {
            music.SetParameter("Boss Door", 1);
            music.SetParameter("Exploration", 1);
            music.SetParameter("Puzzle", 0);
            music.SetParameter("Puzzle Completed", 0);
        }
        else
        {
            music.SetParameter("Boss Fight", 0);
            music.SetParameter("Boss Door", 0);
            music.SetParameter("Puzzle", 0);
        }

        if (activator != null)
        {
            if (activator.GetComponent<LightActivator>().activatorActive == true)
            {
                music.SetParameter("Puzzle Completed", 1);
            }
            else
            {
                music.SetParameter("Puzzle Completed", 0);
            }
        }
    }
}
