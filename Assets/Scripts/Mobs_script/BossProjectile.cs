﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossProjectile : MonoBehaviour
{
    private Rigidbody rb;

    private Vector3 dir;
    private Vector3 iniPos;

    public float force;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        iniPos = transform.position;
    }

    public void CalculateDirection(Vector3 boss)
    {
        dir = (iniPos - boss).normalized;
    }

    private void FixedUpdate()
    {
        rb.AddForce(dir * force);
    }
}
