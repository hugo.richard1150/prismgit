﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossWeakness : MonoBehaviour
{
    [HideInInspector] public float damage;

    public float timeToDestroy;
    private float t;

    private void Start()
    {
        t = 0f;
    }

    public void RaycastTouchBossWeakness()
    {
        t += Time.deltaTime;

        if (t >= timeToDestroy)
        {
            Boss1 boss = GameObject.FindObjectOfType<Boss1>();
            boss.healthBoss -= damage;
            Destroy(gameObject);

            return;
        }
    }
}
