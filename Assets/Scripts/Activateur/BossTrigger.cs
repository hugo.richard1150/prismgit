﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossTrigger : MonoBehaviour
{
    [Header("Boss1 :")]
    public GameObject boss1Stuck;
    public GameObject bossEnemy;
    private Animation boss1Anim;
    private bool boss1AnimOnce;
    private float boss1AnimStuckDuration;

    public bool boss1IsActive;
    public bool cameraToBoss1;
    public float delayAfterAnim;

    private void Start()
    {
        //Boss1
        boss1Anim = boss1Stuck.GetComponent<Animation>();
        boss1Anim.playAutomatically = false;
        boss1AnimOnce = true;
        boss1AnimStuckDuration = boss1Anim.clip.length;
    }

    private void Update()
    {
        //Boss1
        if (boss1IsActive && boss1AnimOnce)
        {
            boss1Anim.Play("Boss1Stuck");
            StartCoroutine(CameraSetToBoss1());
            boss1AnimOnce = false;
        }
    }

    private IEnumerator CameraSetToBoss1()
    {
        cameraToBoss1 = true;
        yield return new WaitForSeconds(boss1AnimStuckDuration);
        cameraToBoss1 = false;
        yield return new WaitForSeconds(delayAfterAnim);
        boss1Stuck.SetActive(false);
        bossEnemy.SetActive(true);
    }
}
