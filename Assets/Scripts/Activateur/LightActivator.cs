﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(FMODUnity.StudioEventEmitter))]
public class LightActivator : MonoBehaviour
{
    public enum ActivatorType { Debug_Ray, Power2, ColorRed, PlatformRotationLeft, PlatformRotationRight, BossLock };
    public ActivatorType type;
    public int subActivated;

    [Header ("GameObjects linked to Activators")]
    public GameObject[] requiredActivators;
    public GameObject[] goEffect;

    [Header("Audio")]
    public FMODUnity.StudioEventEmitter waveSound;
    public FMODUnity.StudioEventEmitter lightSound;

    private Renderer rend;
    private Animator[] anim;

    [Header("Activator Values")]
    public float timeToActive;
    private float t;
    
    public float timeToActive2;
    private float t2;

    public bool rayTouchActivator;

    public bool activatorActive;

    public Material matActivate;
    private Material colorIni;
    private Material colorActivate;

    private float lerp;

    public GameObject[] platformRot;

    [Header ("Particle")]
    public GameObject effetParticule;
    public GameObject effetParticule2;
    private bool boolParticule;
    /*
    [Header("Salle du Boss")]
    public GameObject LR01;
    public GameObject LR02;
    public GameObject LR03;*/


    void Start()
    {
        rend = GetComponent<Renderer>();
        anim = new Animator[goEffect.Length];
        for (int i = 0; i < goEffect.Length; i++)
        {
            anim[i] = goEffect[i].GetComponent<Animator>();
        }

        boolParticule = true;

        colorIni = rend.material;
        colorActivate = matActivate;
    }

    public void Update()
    {
        lerp = Mathf.InverseLerp(0, timeToActive, t);

        // Si la porte du boss est fermée, les interrupteurs intermédiaires sont checkés constamment
        // Pas besoin d'illuminer un interrupteur supplémentaire pour valider l'ouverture
        if (type == ActivatorType.BossLock)
        {
            for (int i = 0; i < anim.Length; i++)
            {
                if (!anim[i].GetBool("Open"))
                {
                    CheckActivator();
                }
                else
                {
                    // Si la porte est ouverte, le check est arrêté
                    return;
                }
            }
        }

        else
        {
            if (rayTouchActivator && !activatorActive)
            {

                if (t < timeToActive)
                {
                    Instantiate(effetParticule2, transform);
                    t += Time.deltaTime;
                    lightSound.SetParameter("IsReflecting", 1);
                    Debug.Log("Light Sound playing");
                }

                if (t >= timeToActive)
                {
                    activatorActive = true;
                    lightSound.SetParameter("IsReflecting", 0);
                    Debug.Log("Light sound stopping");
                    CheckActivator();
                }
            }

            else
            {
                if (t > 0)
                {
                    lightSound.SetParameter("IsReflecting", 0);
                    Debug.Log("Light sound stopping");
                    t -= Time.deltaTime;
                }
            }
        }
        

        if (rayTouchActivator && type == ActivatorType.PlatformRotationLeft)
        {
            //Quand l'interrupteur est sur "PlatformRotationLeft"
            for (int i = 0; i < platformRot.Length; i++)
            {
                PlatformRotationStat platform = platformRot[i].GetComponent<PlatformRotationStat>();
                platformRot[i].transform.Rotate(0, platform.speed * 1 * Time.deltaTime, 0, Space.Self);
            }
        }
        else if (rayTouchActivator && type == ActivatorType.PlatformRotationRight)
        {
            //Quand l'interrupteur est sur "PlatformRotationRight"
            for (int i = 0; i < platformRot.Length; i++)
            {
                PlatformRotationStat platform = platformRot[i].GetComponent<PlatformRotationStat>();
                platformRot[i].transform.Rotate(0, platform.speed * -1 * Time.deltaTime, 0, Space.Self);
            }
        }

        if (!activatorActive)
        {      
            rend.material.SetColor("_EmissiveColor", Color.Lerp(Color.red, Color.green, lerp));
            rend.material.SetFloat("_EmissiveIntensity", Mathf.Lerp(0, 50, lerp));
           
            goEffect[0].GetComponent<Renderer>().material.SetFloat("Vector1_21DBD330", lerp - 0.2f);
            goEffect[0].GetComponent<Renderer>().material.SetColor("Color_C13FA49A", Color.Lerp(new Color(45,0,255),new Color(176,0,255), lerp));
            goEffect[0].GetComponent<Renderer>().material.SetFloat("_EmissiveIntensity", 300);
        }

    }

    public void CheckActivator()
    {
        // Détruit 
        for (int i = 0; i < goEffect.Length; i++)
        {
            if (type == ActivatorType.Debug_Ray)
            {
                rend.material = matActivate;

                if (boolParticule)
                {
                    boolParticule = false;
                    Instantiate(effetParticule, transform);
                }

                if (goEffect[i].tag == "Door")
                {
                    // Destroy(goEffect);
                    goEffect[i].SetActive(false);
                    waveSound.Play();
                }
                else if (goEffect[i].tag == "Bridge")
                {
                    if (!anim[i].GetBool("Open"))
                    {
                        anim[i].SetBool("Open", true);
                    }
                    else if (anim[i].GetBool("Open"))
                    {
                        anim[i].SetBool("Open", false);
                    }                   
                }
                else if (goEffect[i].tag == "Ascenseur")
                {

                    if (!anim[i].GetBool("Up"))
                    {
                        anim[i].SetBool("Up", true);
                    }
                    else
                    {
                        anim[i].SetBool("Up", false);
                    }
                }
                else if (goEffect[i].tag == "Puzzle3Door1")
                {
                    anim[i].SetBool("Ouvert", true);
                }
                else if (goEffect[i].tag == "Puzzle3Door2")
                {
                    anim[i].SetBool("Ouvert", true);
                }
                else if (goEffect[i].tag == "LightGenerator")
                {
                    goEffect[i].SetActive(true);
                }
                else if (goEffect[i].tag == "Yes")
                {
                    goEffect[i].SetActive(false);
                }
                else if (goEffect[i].tag == "ObjectAffectLight")
                {
                    anim[i].SetBool("Open", true);
                }

                /*
                else if (goEffect.tag == "BossLR01")
                {
                    LR01.SetActive(true);
                }
                else if (goEffect.tag == "BossLR02")
                {
                    LR02.SetActive(true);
                }
                else if (goEffect.tag == "BossLR03")
                {
                    LR03.SetActive(true);
                }*/

            }
            // Allumé
            else if (type == ActivatorType.ColorRed)
            {
                rend.material = matActivate;

                if (boolParticule)
                {
                    boolParticule = false;
                    Instantiate(effetParticule, transform);
                }
                Instantiate(effetParticule2, transform);

                gameObject.GetComponent<SubActivator>().activated = true;
            }
            // Demande deux objets activés avant de pouvoir être allumé
            else if (type == ActivatorType.Power2)
            {
                if (requiredActivators.Length != 0)
                {
                    for (int y = 0; y < requiredActivators.Length; y++)
                    {
                        if (requiredActivators[y].GetComponent<SubActivator>().activated && !requiredActivators[y].GetComponent<SubActivator>().hasBeenActivated)
                        {
                            subActivated++;
                            requiredActivators[y].gameObject.GetComponent<SubActivator>().hasBeenActivated = true;
                            Debug.Log("Active Sub Activators: " + subActivated);
                        }
                    }

                    if (subActivated == requiredActivators.Length)
                    {
                        rend.material = matActivate;

                        if (boolParticule)
                        {
                            boolParticule = false;
                            Instantiate(effetParticule, transform);
                        }

                        Instantiate(effetParticule2, transform);

                        if (goEffect[i].tag == "Door")
                        {
                            goEffect[i].SetActive(false);
                        }
                        else if (goEffect[i].tag == "Bridge")
                        {
                            anim[i].SetBool("Open", true);
                        }
                    }
                }
                else if (requiredActivators.Length == 0)
                {
                    Debug.Log("Sub Activators list is empty !");
                    rend.material.SetColor("_EmissiveColor", Color.blue);
                }

            }

            // Porte de boss - S'ouvre quand tous les interrupteurs nécessaires sont activés
            else if (type == ActivatorType.BossLock)
            {
                if (requiredActivators.Length != 0)
                {
                    for (int y = 0; y < requiredActivators.Length; y++)
                    {
                        if (requiredActivators[y].GetComponent<SubActivator>().activated && !requiredActivators[y].GetComponent<SubActivator>().hasBeenActivated)
                        {
                            subActivated++;
                            requiredActivators[y].gameObject.GetComponent<SubActivator>().hasBeenActivated = true;
                            FindObjectOfType<BossTrigger>().boss1IsActive = true;
                            Debug.Log("Active Sub Activators: " + subActivated);
                        }
                    }

                    if (subActivated == requiredActivators.Length)
                    {
                        rend.material = matActivate;

                        if (boolParticule)
                        {
                            boolParticule = false;
                            Instantiate(effetParticule, transform);
                        }

                        Instantiate(effetParticule2, transform);

                        if (goEffect[i].tag == "BossDoor")
                        {
                            anim[i].SetBool("Open", true);
                        }
                    }
                }

                else if (requiredActivators.Length == 0)
                {
                    Debug.Log("Sub Activators list is empty !");
                    rend.material.SetColor("_EmissiveColor", Color.blue);
                }
            }
        }
    }
}
